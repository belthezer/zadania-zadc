# -*- coding: utf-8 -*-

import json
from MapReduce import MapReduce
import glob
import string
import multiprocessing
import operator


def map(item):
    TR = string.maketrans(string.punctuation, ' ' * len(string.punctuation))

    # print multiprocessing.current_process().name, 'czyta', item
    yield item[1], [item[0],item[2],item[3],item[4],item[5],item[6],item[7],item[8],item[9]]



def reduce(line, values):
    return (line, values)


if __name__ == '__main__':
    input_data = []

    with open('records.json', 'rt') as f:
        for line in f:
            input_data.append(json.loads(line))

    mapper = MapReduce(map, reduce)
    word_counts = mapper(input_data)
    slownik = []
    a=0
    for row in word_counts:
        for row1 in row[1]:
            for row2 in row[1]:
                if (row1[0] != row2[0]):
                    if ([row[0]]+row1+row2 in slownik or [row[0]]+row2+row1 in slownik):
                        a+=1
                    else:
                        slownik.append([row[0]]+row1+row2)
        # print json.dumps(row)+"\n"

    for x in slownik:
        print str(x)+"\n"
